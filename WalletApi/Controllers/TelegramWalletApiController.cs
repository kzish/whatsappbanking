﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WalletApi.Models;

using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using Telegram.Bot;
using Models;
using WalletApi.Menus;
using System.IO;
using System.Drawing;

namespace WalletApi.Controllers
{
    /// <summary>
    /// the telegram wallet api
    /// </summary>
    public class TelegramWalletApiController : Controller
    {

        //telegram whatsapp banking_bot
        private static string telegram_access_token = "1286950375:AAFo3ooC2qGiiMRo1w6FjfnR2TbiHLgD3F4";
        private static TelegramBotClient botClient;

        //constructor
        public TelegramWalletApiController()
        {
            if (botClient == null)
            {
                botClient = new TelegramBotClient(telegram_access_token);
                botClient.OnMessage += RecieveMessageCallBack;
                botClient.StartReceiving();
            }
        }

        //receive message callback
        public void RecieveMessageCallBack(object sender, Telegram.Bot.Args.MessageEventArgs e)
        {
            RecieveMessageCallBack message = new RecieveMessageCallBack();
            message.content = e.Message.Text;
            message.from = e.Message.Chat.Id.ToString();//this will act like the whatsapp mobile number (sender)
            WhatsAppWalletApiController.RecieveMessageCallBack(message);
            
        }

        //send a text message
        public static async Task<JsonResult> SendMessageText(string mobileNumber, string message_text)
        {

            try
            {
                Message message = await botClient
                    .SendTextMessageAsync(
                      chatId: mobileNumber,
                      text: message_text,
                      parseMode: ParseMode.Html,
                      disableNotification: false
                  );
                return new JsonResult(new
                {
                    res = "ok",
                    msg = message
                });
            }
            catch (Exception ex)
            {
                return new JsonResult(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }

        //send text message with image
        public static async Task<JsonResult> SendMessageImageAndText(string mobileNumber, string image_content, string caption)
        {
            try
            {
                Message message = await botClient.SendPhotoAsync(
                    chatId: mobileNumber,
                    photo: "https://github.com/TelegramBots/book/raw/master/src/docs/photo-ara.jpg",
                    caption: caption,
                    parseMode: ParseMode.Html
                );
                return new JsonResult(new { res = "err", msg = message });
            }
            catch (Exception ex)
            {
                return new JsonResult(new { res = "err", msg = ex.Message });
            }
        }

        //send an error text message
        public static async Task<JsonResult> SendErrorMessageText(string mobileNumber, string message_text)
        {
            return await SendMessageText(mobileNumber,message_text);
        }

    }
}
