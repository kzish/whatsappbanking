﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WalletApi.Models;
using WalletApi.Models.Wallet;

namespace WalletApi.Controllers
{

    /// <summary>
    /// handles the bill payments
    /// </summary>
    public class BillersController : Controller
    {
        private static string end_point_url = "https://rubieminnovations.com:9001/biller/scpayment/v1";//online
        //private static string end_point_url = "http://localhost:2001/scpayment/v1";//offline
        private static string clientID = "whatsapp_banking";
        private static string clientSecret = "98fcff06-9644-4e3a-9ce6-bb66f1572e54";


        /// <summary>
        /// get the authentication token
        /// </summary>
        /// <returns></returns>
        private static async Task<string> GetAuthToken()
        {
            try
            {
                string auth_token = string.Empty;
                var client = new HttpClient();
                dynamic json_string = await client
                    .GetAsync($"{end_point_url}/Auth/RequestToken?clientID={clientID}&clientSecret={clientSecret}")
                    .Result
                    .Content
                    .ReadAsStringAsync();
                dynamic json = JObject.Parse(json_string);
                auth_token = json.access_token;
                client.Dispose();
                return auth_token;
            }
            catch (Exception ex)
            {
                return "";
            }
        }


        /// <summary>
        /// get the biller types
        /// </summary>
        /// <returns></returns>
        public static async Task<List<string>> GetBillerTypes()
        {
            try
            {
                List<string> biller_types = new List<string>();
                var auth_token = await GetAuthToken();
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", $"Bearer {auth_token}");//add auth token
                string json_string = await client.GetAsync($"{end_point_url}/GetBillerTypes")
                    .Result
                    .Content
                    .ReadAsStringAsync();
                biller_types = JsonConvert.DeserializeObject<List<string>>(json_string);
                client.Dispose();
                return biller_types;
            }
            catch (Exception ex)
            {
                return new List<string>() { };
            }
        }


        /// <summary>
        /// get the billers in category

        /// </summary>
        /// <returns></returns>
        public static async Task<List<string>> GetBillersInCategory(string biller_category)
        {
            try
            {
                List<string> billers = new List<string>();
                var auth_token = await GetAuthToken();
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", $"Bearer {auth_token}");//add auth token
                string json_string = await client.GetAsync($"{end_point_url}/GetBillers?billerCategory={HttpUtility.UrlEncode(biller_category)}")
                    .Result
                    .Content
                    .ReadAsStringAsync();
                dynamic json = JsonConvert.DeserializeObject(json_string);
                if (json.responce_code == "00")
                {
                    foreach (var b in json.data)
                    {
                        billers.Add(b.biller_code.ToString());
                    }
                }
                client.Dispose();
                return billers;
            }
            catch (Exception ex)
            {
                return new List<string>() { };
            }
        }



        /// <summary>
        /// get the selected billers products

        /// </summary>
        /// <returns></returns>
        public static async Task<List<string>> GetSelectedBillersProducts(string biller_code)
        {
            try
            {
                List<string> billers = new List<string>();
                var auth_token = await GetAuthToken();
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", $"Bearer {auth_token}");//add auth token
                string json_string = await client.GetAsync($"{end_point_url}/GetBillerProducts?billerCode={biller_code}")
                    .Result
                    .Content
                    .ReadAsStringAsync();
                dynamic json = JsonConvert.DeserializeObject(json_string);
                if (json.res == "00")
                {
                    int index = 0;
                    foreach (var b in json.data)
                    {
                        index++;
                        //Alignment Component
                        var product = String.Format("{0}{1}{3}"
                            , $"{index} - "
                            , $"{b.name.ToString().ToLower()}..."
                            , b.productCode.ToString().ToLower()
                            , b.price);
                        billers.Add(product);
                    }
                }
                client.Dispose();
                return billers;
            }
            catch (Exception ex)
            {
                return new List<string>() { };
            }
        }

        /// <summary>
        /// get the selected billers products with the id
        /// this method is used by PayBill.ConfirmPayment in order to capture the
        /// product id from the user inputed index
        /// </summary>
        /// <returns></returns>
        public static async Task<string> GetSelectedBillersProductsWithID(string biller_code, int selected_index)
        {
            try
            {
                var product = string.Empty;
                List<string> billers = new List<string>();
                var auth_token = await GetAuthToken();
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", $"Bearer {auth_token}");//add auth token
                string json_string = await client.GetAsync($"{end_point_url}/GetBillerProducts?billerCode={biller_code}")
                    .Result
                    .Content
                    .ReadAsStringAsync();
                dynamic json = JsonConvert.DeserializeObject(json_string);
                if (json.res == "00")
                {
                    int index = 0;
                    foreach (var b in json.data)
                    {
                        index++;
                        if (index == selected_index)
                        {
                            //Alignment Component
                            product += $"{index}|";
                            product += $"{b.name.ToString().ToLower()}|";
                            product += $"{b.productCode.ToString().ToLower()}|";
                            product += $"{b.price}";
                            break;//we found it break
                        }
                    }
                }
                client.Dispose();
                return product;
            }
            catch (Exception ex)
            {
                return "";
            }
        }



        /// <summary>
        /// process the payment, post billercart
        /// </summary>
        /// <returns></returns>
        public static async Task<string> ProcessPayment(mBillerCartRequest billerCartRequest)
        {
            try
            {
                var auth_token = await GetAuthToken();
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", $"Bearer {auth_token}");//add auth token
                string json_string = await client
                .PostAsJsonAsync<mBillerCartRequest>($"{end_point_url}/ProcessPayment", billerCartRequest)
                    .Result
                    .Content
                    .ReadAsStringAsync();
                client.Dispose();
                dynamic json = JsonConvert.DeserializeObject(json_string);
                return json.ToString();
            }
            catch (Exception ex)
            {
                return "Error";
            }
        }



        /// <summary>
        /// ConfirmDetails 
        /// </summary>
        /// <param name="cartRequest"></param>
        /// <returns></returns>
        public static async Task<string> ConfirmDetails(mBillerCartRequest cartRequest)
        {
            try
            {
                var auth_token = await GetAuthToken();
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", $"Bearer {auth_token}");//add auth token
                string json_string = await client
                .PostAsJsonAsync<mBillerCartRequest>($"{end_point_url}/ConfirmDetails", cartRequest)
                    .Result
                    .Content
                    .ReadAsStringAsync();
                client.Dispose();
                dynamic json = JsonConvert.DeserializeObject(json_string);
                if (json.res == "00")
                {
                    return json.data;
                }
                else
                {
                    return "Error confirming details.";
                }
            }
            catch (Exception ex)
            {
                return "Error fetching details";
            }
        }



    }
}
