﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WalletApi.Menus;
using WalletApi.Models;


namespace WalletApi.Controllers
{
    /// <summary>
    /// this is a proxy class that will send messages on the selected platforms
    /// </summary>
    public class WhatsAppWalletApiController : Controller
    {
        //1=telegram
        //2=gupshup
        private static int platform = 1;


        private static string start_text = "/start";//start text


        //fetch the session data
        public static MWalletSession GetSessionData(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            try
            {
                var session_data = db.MWalletSession
                       .Where(i => i.WhatsAppMobileNumber == message.from)
                       .FirstOrDefault();

                if (session_data == null)
                {
                    //create new session and save it
                    session_data = new MWalletSession();
                    session_data.WhatsAppMobileNumber = message.from;
                    session_data.Date = DateTime.Now;
                    session_data.LastMenu = "main";
                    session_data.IndexCounter = 0;
                    db.MWalletSession.Add(session_data);
                    db.SaveChanges();
                }

                return session_data;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                db.Dispose();
            }
        }

        //clear my session to start afresh
        private static void ClearMySession(RecieveMessageCallBack message)
        {
            var db = new dbContext();

            try
            {
                var session_data = db.MWalletSession
                    .Where(i => i.WhatsAppMobileNumber == message.from)
                    .FirstOrDefault();

                if (session_data != null)
                {
                    db.MWalletSession.Remove(session_data);//remove old session to clear session
                }
                //add new blank session
                var new_session = new MWalletSession();
                new_session.WhatsAppMobileNumber = message.from;
                new_session.Date = DateTime.Now;
                new_session.LastMenu = "main";
                new_session.IndexCounter = 0;
                db.MWalletSession.Add(new_session);
                //
                db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
            finally
            {
                db.Dispose();
            }
        }

        //receive message callback and executes the menu
        public static void RecieveMessageCallBack(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            try
            {

                if (message.content == null) return;
                //start_text / clear session
                if (message.content.ToLower() == start_text)
                    ClearMySession(message);

                //get session
                var session_data = GetSessionData(message);
                //check has wallet
                var has_wallet = db.MWallet.Where(i => i.WhatsAppMobileNumber == message.from).Any();


                //welcome menu
                if (!has_wallet && session_data.LastMenu == "main")
                    MenuWelcome.Menu(message);
                else if (session_data.LastMenu == "welcome" && message.content.ToLower() == "1")
                    MenuWelcome.Menu_1(message);
                else if (session_data.LastMenu == "welcome.1" && message.content.ToLower() != "r")
                    MenuWelcome.Menu_1_OTP(message);
                else if (session_data.LastMenu == "welcome.1" && message.content.ToLower() == "r")
                    MenuWelcome.Menu_1(message);//resend the otp

                //main menu
                else if (session_data.LastMenu.Contains("main") && (message.content.ToLower() == "#" || message.content.ToLower() == start_text))
                    MenuMain.Menu(message);//Menu main

                //option 1 main menu (check balance)
                else if (session_data.LastMenu == "main" && message.content.ToLower() == "1")
                    CheckBalance.Menu(message);//check balance

                //option 2 main menu (ministatement)
                else if (session_data.LastMenu == "main" && message.content.ToLower() == "2")
                    MiniStatement.Menu(message, "");//get ministatement
                else if (session_data.LastMenu == "main.2" && (message.content.ToLower() == "n" || message.content.ToLower() == "p"))
                    MiniStatement.Menu(message, message.content.ToLower());//get ministatement

                //option 3 main menu (paybill)
                else if (session_data.LastMenu == "main" && message.content.ToLower() == "3")
                    PayBill.Menu(message);//show the categories, navigate the categories, select a biller category
                else if (session_data.LastMenu == "main.3" && (message.content.ToLower() == "n" || message.content.ToLower() == "p"))
                    PayBill.Menu(message);//show the categories, navigate the categories, select a biller category

                else if (session_data.LastMenu == "main.3")//no nav sepcified
                    PayBill.Menu_1(message);//accept the selected biller category
                                            // sends you to 3.2
                else if (session_data.LastMenu == "main.3.category" && (message.content.ToLower() == "n" || message.content.ToLower() == "p"))
                    PayBill.DiplayBillersInSelectedCategory(message);//display and navigate billers in selected category

                else if (session_data.LastMenu == "main.3.category")//category selected no nav provided, indicates attempt to select biller
                    PayBill.SelectTheBillerFromTheCategories(message);//select the biller and store into db
                else if (session_data.LastMenu == "main.3.category.biller" && (message.content.ToLower() == "n" || message.content.ToLower() == "p"))
                    PayBill.ShowBillersProducts(message);//Display selected billers products accepts navigation,
                                                         //and prompts the user to enter the details for payment

                else if (session_data.LastMenu == "main.3.category.biller")//no nav provided, indicates product has been selected
                    PayBill.ConfirmPayment(message);//display confirm payment and prompt for pin
                else if (session_data.LastMenu == "main.3.category.biller.confirm")
                    PayBill.ExecutePayment(message);//accept pin and execute the payment


                //option 4 main menu (transfer funds)
                else if (session_data.LastMenu == "main" && message.content.ToLower() == "4")
                    TransferFunds.Menu(message);//transfer funds menu
                else if (session_data.LastMenu == "main.4")//index
                    TransferFunds.Menu_1(message);//accept the number to transfer funds to
                else if (session_data.LastMenu == "main.4.1")
                    TransferFunds.Menu_2(message);//accept the amount to transfer and validate that there are sufficient funds
                else if (session_data.LastMenu == "main.4.2")
                    TransferFunds.Menu_3(message);//recieve confirm transfer, and display request pin
                else if (session_data.LastMenu == "main.4.3")
                    TransferFunds.Menu_4(message);//recieve pin if correct execute transfer

                //option 5 main menu (wallet services)
                else if (session_data.LastMenu == "main" && message.content.ToLower() == "5")
                    WalletServices.Menu(message, "");//Wallet services main menu
                else if (session_data.LastMenu == "main.5" && (message.content.ToLower() == "n" || message.content.ToLower() == "p"))
                    WalletServices.Menu(message, message.content.ToLower());//Wallet services main menu

                //cash out
                else if (session_data.LastMenu == "main.5" && message.content.ToLower() == "1")
                    CashOut.Menu_1(message);//request agent code
                else if (session_data.LastMenu == "main.5.1.1")
                    CashOut.Menu_2(message);//verify agent code and request amount to cash out
                else if (session_data.LastMenu == "main.5.1.2")
                    CashOut.Menu_3(message);//verify sufficient funds and proceed to cash out
                else if (session_data.LastMenu == "main.5.1.3")
                    CashOut.Menu_4(message);//verify pin and execute


                //change pin
                else if (session_data.LastMenu == "main.5" && message.content.ToLower() == "2")
                    ChangePin.Menu_1(message);//display request old pin
                else if (session_data.LastMenu == "main.5.2.1")
                    ChangePin.Menu_2(message);//display request new pin
                else if (session_data.LastMenu == "main.5.2.2")
                    ChangePin.Menu_3(message);//display request old pin


                //ecocash to rubiwallet topup
                else if (session_data.LastMenu == "main.5" && message.content.ToLower() == "8")
                    EcoCashTORubiWallet.Menu_1(message);//request topup amount
                else if (session_data.LastMenu == "main.5.8")
                    EcoCashTORubiWallet.Menu_2(message);//accept number and topup amount, validate and execute



                //option 6 buy airtime
                else if (session_data.LastMenu == "main" && message.content.ToLower() == "6")
                    BuyAirtime.Menu(message);//display prompt to enter account and amount
                else if (session_data.LastMenu == "main.6")
                    BuyAirtime.Menu_1(message);//display confirmation
                else if (session_data.LastMenu == "main.6.1")
                    BuyAirtime.ExecutePayment(message);//confirm pin and exec payment



                //option 7 buy zesa
                else if (session_data.LastMenu == "main" && message.content.ToLower() == "7")
                    BuyZesa.Menu(message);//display prompt to enter account and amount
                else if (session_data.LastMenu == "main.7")
                    BuyZesa.Menu_1(message);//display confirmation
                else if (session_data.LastMenu == "main.7.1")
                    BuyZesa.ExecutePayment(message);//confirm pin and exec payment


                else//show the last successful menu
                    WhatsAppWalletApiController
                        .SendMessageText(message.from, $"invalid response, try again or enter '{start_text}' to start return to main menu").Wait();
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
            }
            finally
            {
                db.Dispose();
            }
        }

        //send a text message
        public static async Task<JsonResult> SendMessageText(string mobileNumber, string message_text)
        {
            if (platform == 1) await TelegramWalletApiController.SendMessageText(mobileNumber, message_text);
            if (platform == 2)
            {
                message_text = message_text.Replace("&", "AND");//whatsapp does not like ampersand symbol
                await GupshupWalletApiController.SendMessageText(mobileNumber, message_text);
            }
            return new JsonResult(new { });
        }

        //send text message with image
        public static async Task<JsonResult> SendMessageImageAndText(string mobileNumber, string image_content, string caption)
        {
            if (platform == 1) await TelegramWalletApiController.SendMessageImageAndText(mobileNumber, image_content, caption);
            if (platform == 2)
            {
                caption = caption.Replace("&", "AND");//whatsapp does not like ampersand symbol
                await GupshupWalletApiController.SendMessageImageAndText(mobileNumber, image_content, HttpUtility.HtmlEncode(caption));
            }
            return new JsonResult(new { });
        }

        //send an error text message
        public static async Task<JsonResult> SendErrorMessageText(string mobileNumber, string message_text)
        {
            //if (platform == 1) await TelegramWalletApiController.SendErrorMessageText(mobileNumber, message_text);
            //if (platform == 2) await GupshupWalletApiController.SendErrorMessageText(mobileNumber, message_text);
            //send errot through telegram always
            await TelegramWalletApiController.SendErrorMessageText("586097924", message_text);
            return new JsonResult(new { });
        }




    }
}
