﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WalletApi.Models;

using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using Telegram.Bot;
using Models;
using WalletApi.Menus;
using System.IO;
using System.Drawing;
using WalletApi.Models.Wallet;
using System.Net;

namespace WalletApi.Controllers
{
    /// <summary>
    /// the telegram wallet api
    /// </summary>
    public class GupshupWalletApiController : Controller
    {

        //receive message callback
        [HttpPost("RecieveMessageCallBack")]
        public void RecieveMessageCallBack([FromBody] GupShupRecieveMessageCallBack e)
        {
            RecieveMessageCallBack message = new RecieveMessageCallBack();
            message.content = e.payload.payload.text;
            message.from = e.payload.source;//this will act like the whatsapp mobile number (sender)
            WhatsAppWalletApiController.RecieveMessageCallBack(message);
        }

        //send a text message
        public static async Task<bool> SendMessageText(string mobileNumber, string message_text)
        {
            try
            {

                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Accept", "application/x-www-form-urlencoded");
                client.DefaultRequestHeaders.Add("apikey", $"{Globals.gupshup_apikey}");
                client.DefaultRequestHeaders.Add("cache-control", "no-cache");
                client.DefaultRequestHeaders.Add("Cache-Control", "no-cache");

                var string_content = new StringContent($"channel=whatsapp&source={Globals.gupshup_source}&destination={mobileNumber}&message={message_text}&src.name=RubiWallet"
                    , Encoding.UTF8,
                    "application/x-www-form-urlencoded");

                var response = await client.PostAsync($"{Globals.gupshup_end_point}", string_content).Result.Content.ReadAsStringAsync();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        //send text message with image
        public static async Task<bool> SendMessageImageAndText(string mobileNumber, string image_url, string caption)
        {
            try
            {

                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Accept", "application/x-www-form-urlencoded");
                client.DefaultRequestHeaders.Add("apikey", $"{Globals.gupshup_apikey}");
                client.DefaultRequestHeaders.Add("cache-control", "no-cache");
                client.DefaultRequestHeaders.Add("Cache-Control", "no-cache");

                dynamic json = new JObject();
                json.type = "image";
                json.originalUrl = image_url;
                json.previewUrl = image_url;
                json.caption = caption;

                string json_message = JsonConvert.SerializeObject(json);
                var string_content = new StringContent($"channel=whatsapp&source={Globals.gupshup_source}&destination={mobileNumber}&message={json_message}"
                    , Encoding.UTF8,
                    "application/x-www-form-urlencoded");

                var response = await client.PostAsync($"{Globals.gupshup_end_point}", string_content).Result.Content.ReadAsStringAsync();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        //send an error text message
        public static async Task<bool> SendErrorMessageText(string mobileNumber, string message_text)
        {
            return await SendMessageText(mobileNumber, message_text);
        }

    }
}
