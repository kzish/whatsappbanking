﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting.Internal;
using Models;
using WalletApi.Controllers;
using WalletApi.Models;

namespace WalletApi.Menus
{
    public class MenuWelcome
    {
        //welcome menu
        public static string Menu(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            try
            {
                //send welcome menu
                var menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuWelcome/menu_welcome.txt");
                var image_path = $"{Globals.base_path}/Raw/welcome.png";
                var response = WhatsAppWalletApiController.SendMessageImageAndText(message.from, image_path, menu).Result;
                var session_data = db.MWalletSession.Find(message.from);
                session_data.LastMenu = "welcome";
                db.SaveChanges();
                return response.Value.ToString();
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
                return "";
            }
            finally
            {
                db.Dispose();
            }
        }

        //option 1
        public static string Menu_1(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            try
            {
                var session_data = db.MWalletSession.Find(message.from);
                //send welcome menu
                var menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuWelcome/menu_1.txt");
                //save session data
                session_data.WhatsAppMobileNumber = message.from;
                session_data.LastMenu = "welcome.1";
                session_data.Date = DateTime.Now;
                db.SaveChanges();//update the session information
                Helpers.GenerateAndSendOtp(message.from);//send the otp via the sms
                var response = WhatsAppWalletApiController.SendMessageText(message.from, menu).Result;
                db.Dispose();
                return response.Value.ToString();
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
                return "";
            }
            finally
            {
                db.Dispose();
            }
        }


        //option 1 - entered otp
        public static string Menu_1_OTP(RecieveMessageCallBack message)
        {
            var db = new dbContext();

            try
            {
                var menu = string.Empty;
                var otp = db.MOtpTokens.Where(i =>
                i.WhatsAppMobileNumber == message.from
                && i.Otp == message.content)
                    .FirstOrDefault();
                if (otp == null)//otp does not exist
                {
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", "This otp is invalid, re-Enter the OTP");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();//send
                }
                else//otp exists
                {
                    //otp has not yet expired
                    if (otp.ExpirationDate >= DateTime.Now)
                    {
                        //create wallet, remove otp
                        db.MOtpTokens.Remove(otp);
                        var wallet = new MWallet();
                        wallet.WhatsAppMobileNumber = message.from;
                        wallet.Balance = 0;
                        wallet.DateCreated = DateTime.Now;
                        wallet.Pin = "0000";//default pin
                        db.MWallet.Add(wallet);

                        //send congrats message
                        menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuWelcome/congratulations_wallet_created.txt");
                        WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();

                        //send the main menu also
                        menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/menu_main.txt");
                        var image_main = $"{Globals.base_path}/Raw/main_menu.png";
                        WhatsAppWalletApiController.SendMessageImageAndText(message.from, image_main, menu).Wait();//send
                        var session_data = db.MWalletSession.Find(message.from);
                        session_data.LastMenu = "main";//we are now on the main menu
                        db.SaveChanges();//update the session information
                    }
                    else//otp has now expired
                    {
                        menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                        menu = menu.Replace("{{error_message}}", $"This otp has expired {Environment.NewLine}Press *r* to resend OTP.");
                        WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();//send
                    }
                }

                return "";
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
                return "";
            }
            finally
            {
                db.Dispose();
            }
        }



    }
}
