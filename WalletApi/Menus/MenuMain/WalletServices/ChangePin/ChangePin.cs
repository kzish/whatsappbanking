﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WalletApi.Controllers;
using WalletApi.Models;

namespace WalletApi.Menus
{
    public class ChangePin
    {

        private static int array_limit = 5;

        /// <summary>
        /// display request old pin
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static string Menu_1(RecieveMessageCallBack message)
        {
            var db = new dbContext();

            try
            {
                var menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/WalletServices/ChangePin/enter_current_pin.txt");
                var response = WhatsAppWalletApiController.SendMessageText(message.from, menu).Result;
                var session_data = db.MWalletSession.Find(message.from);
                session_data.Date = DateTime.Now;
                session_data.LastMenu = "main.5.2.1";
                db.Entry(session_data).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();//update session
                db.Dispose();
                return response.Value.ToString();
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
                return "";
            }
            finally
            {
                db.Dispose();
            }
        }


        /// <summary>
        /// check the current pin is valid and send the next menu
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static string Menu_2(RecieveMessageCallBack message)
        {
            var db = new dbContext();

            try
            {
                var session_data = db.MWalletSession.Find(message.from);
                var my_wallet = db.MWallet.Find(message.from);
                var menu = string.Empty;


                //incorrect pin
                if (message.content != my_wallet.Pin)
                {
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", "Incorrect pin");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    //prev menu
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/WalletServices/ChangePin/enter_current_pin.txt");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    return "failed";
                }
                //correct pin
                else
                {
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/WalletServices/ChangePin/enter_new_pin.txt");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    session_data.Date = DateTime.Now;
                    session_data.LastMenu = "main.5.2.2";
                    db.Entry(session_data).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                    db.SaveChanges();//update session
                    db.Dispose();
                    return "sent";
                }

            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
                return "";
            }
            finally
            {
                db.Dispose();
            }
        }


        /// <summary>
        /// validate new pin and update the pin
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static string Menu_3(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            try
            {
                var session_data = db.MWalletSession.Find(message.from);
                var my_wallet = db.MWallet.Find(message.from);
                var menu = string.Empty;

                //pin must be 4 digits
                if (message.content.Length != 4)
                {
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", "Pin must be 4 digits");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    //prev menu
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/WalletServices/ChangePin/enter_new_pin.txt");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    return "failed";
                }

                //must not be the same as the old pin
                if (message.content == my_wallet.Pin)
                {
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", "Pin cannot be the same as the old pin");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    //prev menu
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/WalletServices/ChangePin/enter_new_pin.txt");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    return "failed";
                }

                //parse the pin to ensure that they are digits
                if (!message.content.All(Char.IsDigit))
                {
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", "Pin must contain only numbers");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    //prev menu
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/WalletServices/ChangePin/enter_new_pin.txt");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    return "failed";
                }

                my_wallet.Pin = message.content;
                db.Entry(my_wallet).State = Microsoft.EntityFrameworkCore.EntityState.Modified;

                menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/WalletServices/ChangePin/successfully_changed_pin.txt");
                WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();

                MenuMain.Menu(message);//go to main menu
                db.SaveChanges();
                db.Dispose();
                return "sent";
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
                return "";
            }
            finally
            {
                db.Dispose();
            }
        }

    }
}


