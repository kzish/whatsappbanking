﻿using Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WalletApi.Controllers;
using WalletApi.Models;

namespace WalletApi.Menus
{
    public class EcoCashTORubiWallet
    {

        private static int array_limit = 5;

        /// <summary>
        /// display request old pin
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static string Menu_1(RecieveMessageCallBack message)
        {
            var db = new dbContext();

            try
            {
                var menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/WalletServices/Ecocash_TO_RubiWallet/main_menu.txt");
                var response = WhatsAppWalletApiController.SendMessageText(message.from, menu).Result;
                var session_data = db.MWalletSession.Find(message.from);
                session_data.Date = DateTime.Now;
                session_data.LastMenu = "main.5.8";
                db.Entry(session_data).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();//update session
                db.Dispose();
                return response.Value.ToString();
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
                return "";
            }
            finally
            {
                db.Dispose();
            }
        }


        /// <summary>
        /// accept eccash number and topup amount and validate minimum
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static string Menu_2(RecieveMessageCallBack message)
        {
            var db = new dbContext();

            try
            {
                var session_data = db.MWalletSession.Find(message.from);
                var menu = string.Empty;

                int items_length = message.content.Split('#').Length;

                //incorrect format
                if (items_length != 2)
                {
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", "Invalid format");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    return "failed";
                }

                decimal topup_amount = 0;
                //invalid amount
                if (!decimal.TryParse(message.content.Split('#')[1], out topup_amount))
                {
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", "Invalid amount");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    return "failed";
                }

                decimal minimum_topup_amount = Helpers.ecocash_minimum_topup_amount;
                //amount less than minimum
                if (topup_amount < minimum_topup_amount)
                {
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", $"Top up minimum is {minimum_topup_amount}");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    return "failed";
                }

                //all good
                else
                {
                    var ecocash_phone_number = message.content.Split('#')[0];
                    var execute_ecocash_payment = Helpers.ExecuteEcocashPayment(ecocash_phone_number, topup_amount);
                    //success
                    if (execute_ecocash_payment)
                    {
                        //topup wallet
                        var my_wallet = db.MWallet.Find(message.from);
                        my_wallet.Balance += topup_amount;
                        db.SaveChanges();
                        menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/WalletServices/Ecocash_TO_RubiWallet/successfully_topped_up_wallet.txt");
                        menu = menu.Replace("{{balance}}", my_wallet.Balance.ToString("0.00"));
                        WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                        Menus.MenuMain.Menu(message);//send back to main menu
                        return "sent";
                    }
                    //error
                    else
                    {
                        menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                        menu = menu.Replace("{{error_message}}", $"Ecocash error occurred.");
                        menu = menu.Replace("*Try again.*", "");
                        WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                        Menus.MenuMain.Menu(message);//send back to main menu
                        return "failed";
                    }


                }

            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
                return "";
            }
            finally
            {
                db.Dispose();
            }
        }




    }
}


