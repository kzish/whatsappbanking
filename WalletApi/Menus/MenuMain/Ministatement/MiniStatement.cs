﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WalletApi.Controllers;
using WalletApi.Models;

namespace WalletApi.Menus
{
    public class MiniStatement
    {
        private static int array_limit = 5;

        /// <summary>
        /// display ministatement
        /// </summary>
        /// <param name="message">the recieved message</param>
        /// <param name="direction">either n for next or p for previous,</param>
        /// <returns></returns>
        public static string Menu(RecieveMessageCallBack message, string direction)
        {
            try
            {
                var db = new dbContext();
                var menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/MiniStatement/mini_statement.txt");

                var session_data = db.MWalletSession.Find(message.from);

                //number of pages
                int pages = db.MTransactions.Count(i => i.WhatsAppMobileNumber == message.from) / array_limit;

                if (direction == "n" && (session_data.IndexCounter < pages)) session_data.IndexCounter++;//go next
                if (direction == "p" && (session_data.IndexCounter >= 1)) session_data.IndexCounter--;//go prev

                //pagination
                var sub_transactions = db.MTransactions
                    .Where(i => i.WhatsAppMobileNumber == message.from)
                    .OrderByDescending(i => i.Date)
                    .Skip(array_limit * session_data.IndexCounter)
                    .Take(array_limit)
                    .ToList();

                var list = string.Empty;
                int index = (array_limit * session_data.IndexCounter);//start here


                foreach (var trans in sub_transactions)
                {
                    index++;//dont start at zero
                    if (trans.From == message.from)//outgoing transaction
                    {
                        list += $"{index}) {trans.Date.ToString("dd/MM/yyyy")} 🔻 {trans.To} ▪ {trans.Amount.ToString("0.00")} {Environment.NewLine}";
                    }
                    else//incoming transaction
                    {
                        list += $"{index}) {trans.Date.ToString("dd/MM/yyyy")} 🔼 {trans.From} ▪ {trans.Amount.ToString("0.00")}{Environment.NewLine}";
                    }
                }

                menu = menu.Replace("{{list}}", list);
                menu = menu.Replace("{{pager}}", $"{session_data.IndexCounter + 1} of {pages + 1}");
                var response = WhatsAppWalletApiController.SendMessageText(message.from, menu).Result;


                session_data.Date = DateTime.Now;
                session_data.LastMenu = "main.2";
                db.Entry(session_data).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();//update session
                db.Dispose();

                return response.Value.ToString();
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
                return "";
            }
        }

    }
}
