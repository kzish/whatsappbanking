﻿using Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WalletApi.Controllers;
using WalletApi.Models;

namespace WalletApi.Menus
{
    public class PayBill
    {
        private static int array_limit = 5;

        /// <summary>
        /// display biller categories
        /// </summary>
        /// <param name="message">the recieved message</param>
        /// <param name="direction">either n for next or p for previous,</param>
        /// <returns></returns>
        public static string Menu(RecieveMessageCallBack message)
        {
            var db = new dbContext();

            try
            {
                var menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/PayBill/main_menu.txt");

                var session_data = db.MWalletSession.Find(message.from);
                var biller_types = BillersController.GetBillerTypes().Result;
                //number of pages
                int pages = biller_types.Count / array_limit;

                if (message.content.ToLower() == "n" && (session_data.IndexCounter < pages)) session_data.IndexCounter++;//go next
                if (message.content.ToLower() == "p" && (session_data.IndexCounter >= 1)) session_data.IndexCounter--;//go prev

                //pagination
                var sub_biller_types = biller_types
                    .Skip(array_limit * session_data.IndexCounter)
                    .Take(array_limit)
                    .ToList();

                var list = string.Empty;
                int index = (array_limit * session_data.IndexCounter);//start here


                foreach (var item in sub_biller_types)
                {
                    index++;//dont start at zero
                    list += $"{index} - {item}{Environment.NewLine}";
                }

                menu = menu.Replace("{{list}}", list);
                menu = menu.Replace("{{pager}}", $"{session_data.IndexCounter + 1} of {pages + 1}");
                var response = WhatsAppWalletApiController.SendMessageText(message.from, menu).Result;


                session_data.Date = DateTime.Now;
                session_data.LastMenu = "main.3";
                db.Entry(session_data).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();//update session
                db.Dispose();

                return response.Value.ToString();
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
                return "";
            }
            finally
            {
                db.Dispose();
            }
        }


        /// <summary>
        /// accept the selected biller category
        /// and immediatly display the selected biller category items
        /// </summary>
        /// <param name="message">the recieved message</param>
        /// <returns></returns>
        public static string Menu_1(RecieveMessageCallBack message)
        {
            var db = new dbContext();

            try
            {
                var menu = string.Empty;
                List<string> biller_types = null;

                //input must be a number
                if (!message.content.All(Char.IsDigit))
                {
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", "Input must be a number");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    //prev menu
                    PayBill.Menu(message);//this is compatible with the current message
                    return "fail";
                }
                else//input is a valid number 
                {
                    biller_types = BillersController.GetBillerTypes().Result;
                    int selected_biller_type = int.Parse(message.content);
                    //selection not in range
                    if (selected_biller_type <= 0 || selected_biller_type > (biller_types.Count))
                    {
                        //invalid selection
                        menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                        menu = menu.Replace("{{error_message}}", $"Input must be between 1 and {biller_types.Count}");
                        WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                        //prev menu
                        PayBill.Menu(message);//this is compatible with the current message
                        return "fail";
                    }
                    else//selection in range
                    {
                        //minus 1 because array begins at 0
                        //save session data
                        Helpers.WriteSessionData(message, "biller_category", biller_types[selected_biller_type - 1]);//store the selected biller category
                        //
                        var session_data = db.MWalletSession.Find(message.from);
                        session_data.LastMenu = "main.3.category";//we have selected a category
                        session_data.IndexCounter = 0;//reset before leaving
                        db.SaveChanges();
                        //immediatly show the menu for the selected biller category
                        PayBill.DiplayBillersInSelectedCategory(message);//sends you to the next menu
                        return "success";
                    }

                }
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
                return "";
            }
            finally
            {
                db.Dispose();
            }
        }


        /// <summary>
        /// display billers in the selected category
        /// method called in Menu_1
        /// navigates and displays billers in selected category
        /// </summary>
        /// <param name="message">the recieved message</param>
        /// <param name="direction">either n for next or p for previous,</param>
        /// <returns></returns>
        public static string DiplayBillersInSelectedCategory(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            var session_data_data = Helpers.ReadSessionData(message);
            try
            {

                var menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/PayBill/display_billers_in_category.txt");

                string biller_category = session_data_data.biller_category.ToString();
                menu = menu.Replace("{{category}}", biller_category);
                var billers = BillersController.GetBillersInCategory(biller_category).Result;
                //number of pages
                int pages = billers.Count / array_limit;
                
                var session_data = db.MWalletSession.Find(message.from);

                if (message.content.ToLower() == "n" && (session_data.IndexCounter < pages)) session_data.IndexCounter++;//go next
                if (message.content.ToLower() == "p" && (session_data.IndexCounter >= 1)) session_data.IndexCounter--;//go prev

                //pagination
                var sub_billers = billers
                    .Skip(array_limit * session_data.IndexCounter)
                    .Take(array_limit)
                    .ToList();

                var list = string.Empty;
                int index = (array_limit * session_data.IndexCounter);//start here


                foreach (var item in sub_billers)
                {
                    index++;//dont start at zero
                    list += $"{index} - {item}{Environment.NewLine}";
                }

                menu = menu.Replace("{{list}}", list);
                menu = menu.Replace("{{pager}}", $"{session_data.IndexCounter + 1} of {pages + 1}");
                var response = WhatsAppWalletApiController.SendMessageText(message.from, menu).Result;


                return response.Value.ToString();
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
                return "";
            }
            finally
            {
                db.Dispose();
            }
        }


        /// <summary>
        /// select the biller from the categories
        /// store biller in db
        /// </summary>
        /// <param name="message"></param>
        /// <param name="direction"></param>
        public static void SelectTheBillerFromTheCategories(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            var session_data_data = Helpers.ReadSessionData(message);
            try
            {
                string billerCategory = session_data_data.biller_category.ToString();
                List<string> billers = BillersController.GetBillersInCategory(billerCategory).Result;
                int pages = billers.Count / array_limit;
                var menu = string.Empty;

                //must be a number
                if (!message.content.All(char.IsDigit))
                {
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", "Input must be a number");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    return;
                }
                //must be in range
                int selected_biller = int.Parse(message.content);
                if (selected_biller <= 0 || selected_biller > billers.Count)
                {
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", $"Input must be between 1 and {billers.Count}");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    return;
                }
                //valid
                //minus 1 becuase array begins at 0
                Helpers.WriteSessionData(message, "biller_code", billers[selected_biller - 1]);//save selected biler_code
                ShowBillersProducts(message);//show the next menu immediatly
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();//send error
            }
            finally
            {
                db.Dispose();
            }
        }

        /// <summary>
        /// show selected biller's products
        /// accept navigation
        /// </summary>
        /// <param name="message"></param>
        /// <param name="direction"></param>
        public static void ShowBillersProducts(RecieveMessageCallBack message)
        {
            var db = new dbContext();

            try
            {
                var session_data_data = Helpers.ReadSessionData(message);
                var session_data = db.MWalletSession.Find(message.from);
                string billerCode = session_data_data.biller_code.ToString();
                List<string> billerProducts = BillersController.GetSelectedBillersProducts(billerCode).Result;
                int pages = billerProducts.Count / array_limit;
                if (message.content.ToLower() == "n" && (session_data.IndexCounter < pages)) session_data.IndexCounter++;//go next
                if (message.content.ToLower() == "p" && (session_data.IndexCounter >= 1)) session_data.IndexCounter--;//go prev
                var sub_products = billerProducts
                        .Skip(array_limit * session_data.IndexCounter)
                        .Take(array_limit)
                        .ToList();
                var list = string.Empty;
                int index = (array_limit * session_data.IndexCounter);//start here
                foreach (var product in sub_products)
                {
                    list += $"{product}{Environment.NewLine}";
                }
                session_data.Date = DateTime.Now;
                session_data.LastMenu = "main.3.category.biller";//remain here
                db.Entry(session_data).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();
                db.Dispose();
                var menu = System.IO.File
                    .ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/PayBill/display_selected_billers_products.txt");
                menu = menu.Replace("{{biller}}", billerCode);
                menu = menu.Replace("{{list}}", list);
                menu = menu.Replace("{{pager}}", $"{session_data.IndexCounter + 1} of {pages + 1}");
                WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();//send the products
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();//send error
            }
            finally
            {
                db.Dispose();
            }
        }


        /// <summary>
        /// accept payer details
        /// display confirmation details
        /// </summary>
        /// <param name="message"></param>
        /// <param name="direction"></param>
        public static void ConfirmPayment(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            //var session_data = db.MWalletSession.Find(message.from);
            var my_wallet = db.MWallet.Find(message.from);
            var menu = string.Empty;

            //
            string payer_account_no = String.Empty;
            string payer_product_id = string.Empty;
            decimal qty = 0;
            string payer_address = string.Empty;
            string product_name = string.Empty;
            decimal price = 0;// the price of the product i am buying

            //payer details must have 4 fields
            int fields_length = message.content.Split('#').Length;
            if (fields_length == 4)
            {
                payer_account_no = message.content.Split('#')[1].Trim();
                payer_product_id = message.content.Split('#')[0].Trim();

                //selected id mut be an integer
                if (!payer_product_id.All(Char.IsDigit))
                {
                    //invlid id selection
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", "Invalid ID selected, must be a number greater than 0");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();//send
                    db.Dispose();
                    return;
                }

                //qty must be more than 0
                decimal.TryParse(message.content.Split('#')[2].Trim(), out qty);
                if (qty <= 0)
                {
                    //failed to parse the qty
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", "Invalid qty amount, must be a number greater than 0");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();//send
                    db.Dispose();
                    return;
                }

                payer_address = message.content.Split('#')[3].Trim();
                var session_data_data = Helpers.ReadSessionData(message);
                //attempt to get the product price from the selection
                var billerCode = session_data_data.biller_code.ToString();
                string selected_biller_product = BillersController.GetSelectedBillersProductsWithID(billerCode, int.Parse(payer_product_id)).Result;
                //if selected_biller_product is null then the product was not found
                if (string.IsNullOrEmpty(selected_biller_product))
                {
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", "Product ID is invalid");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();//send
                    db.Dispose();
                    return;
                }

                //filter the elements of the product
                product_name = selected_biller_product.Split('|')[1].Trim();
                payer_product_id = selected_biller_product.Split('|')[2].Trim();//set the new and actual product code not its index
                price = decimal.Parse(selected_biller_product.Split('|')[3].Trim());

                //these must never be zero
                if (price <= 0) price = 1;
                if (qty <= 0) qty = 1;
                //
                decimal total_amount_to_pay = (price * qty);
                var transaction_fee = Helpers.GetTransactionFee("bill_payment", total_amount_to_pay);

                //balance must be sufficient
                if (my_wallet.Balance < (total_amount_to_pay + transaction_fee))
                {
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", "Insufficient balance to make this transaction");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();//send
                    //dont go to main menu yet but allow user to try another transaction amount
                    db.Dispose();
                    return;
                }
                else
                {
                    //save data into session, and request pin to confirm payment
                    Helpers.WriteSessionData(message, "product_name", product_name);
                    Helpers.WriteSessionData(message, "account_number", payer_account_no);
                    Helpers.WriteSessionData(message, "product_id", payer_product_id);
                    Helpers.WriteSessionData(message, "qty", qty.ToString("0.00"));
                    Helpers.WriteSessionData(message, "address", payer_address);
                    Helpers.WriteSessionData(message, "unit_price", price.ToString("0.00"));
                    //
                    session_data_data = Helpers.ReadSessionData(message);
                    //
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/PayBill/display_confirm_purchase_and_request_pin.txt");
                    menu = menu.Replace("{{biller}}", session_data_data.biller_code.ToString());
                    menu = menu.Replace("{{product_name}}", product_name);
                    menu = menu.Replace("{{product_code}}", session_data_data.product_name.ToString());
                    menu = menu.Replace("{{payer_account_no}}", session_data_data.account_number.ToString());
                    menu = menu.Replace("{{qty}}", session_data_data.qty.ToString());
                    menu = menu.Replace("{{price}}", session_data_data.unit_price.ToString());
                    menu = menu.Replace("{{address}}", session_data_data.address.ToString());
                    menu = menu.Replace("{{total}}", (total_amount_to_pay).ToString("0.00"));
                    var session_data = db.MWalletSession.Find(message.from);
                    session_data.LastMenu = "main.3.category.biller.confirm";//successful confirmation
                    db.SaveChanges();
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();//send
                }
            }
            else//incorrect format
            {
                menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                menu = menu.Replace("{{error_message}}", "Payer details have incorrect format");
                WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();//send
                db.Dispose();
                return;
            }
        }


        /// <summary>
        /// Accepts pin
        /// Validates pin
        /// Deducts the amount
        /// Saves the transaction
        /// </summary>
        /// <param name="message"></param>
        /// <param name="direction"></param>
        public static void ExecutePayment(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            try
            {
                //var session_data = db.MWalletSession.Find(message.from);
                var my_wallet = db.MWallet.Find(message.from);
                var menu = string.Empty;

                if (message.content != my_wallet.Pin)
                {
                    //invalid pin
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", "Incorrect pin");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();//send
                }
                else
                {
                    //pin is valid continue

                    ///* post bilerCart to the server
                    /// deduct the (amount + fees from client wallet)
                    var session_data_data = Helpers.ReadSessionData(message);
                    decimal qty = decimal.Parse(session_data_data.qty.ToString());
                    decimal price = decimal.Parse(session_data_data.unit_price.ToString());
                    //these must never be zero
                    if (price <= 0) price = 1;
                    if (qty <= 0) qty = 1;
                    //
                    var amount_to_pay = (qty * price);

                    //execute the paybill
                    var billerCartRequest = Helpers.CreateBillerCart(message);
                    string json_response = BillersController.ProcessPayment(billerCartRequest).Result;
                    dynamic payBillResponse = JsonConvert.DeserializeObject(json_response);

                    //if success
                    if (payBillResponse.res.ToString().Equals("00"))
                    {
                        var transaction_fee = Helpers.GetTransactionFee("bill_payment", amount_to_pay);

                        //deduct the amount to cash out
                        my_wallet.Balance -= amount_to_pay;
                        //deduct the fee charged
                        my_wallet.Balance -= transaction_fee;

                        var biller_code = session_data_data.biller_code.ToString();

                        //record this transaction
                        var deduction_transaction = new MTransactions();
                        deduction_transaction.WhatsAppMobileNumber = message.from;
                        deduction_transaction.Date = DateTime.Now;
                        deduction_transaction.Amount = amount_to_pay;//dont record the fee here, only record the deduction amount
                        deduction_transaction.EnumTransactionType = db.ETransactionTypes.Where(i => i.TransactionType == "bill_payment").First().Id;
                        deduction_transaction.From = message.from;//from me to biller
                        deduction_transaction.To = biller_code;//reciever
                        db.MTransactions.Add(deduction_transaction);//save transaction

                        //record also the transaction fee into the db
                        var mWalletTransactionFees = new MWalletTransactionFees();
                        mWalletTransactionFees.Date = DateTime.Now;
                        mWalletTransactionFees.WhatsAppMobileNumber = message.from;
                        mWalletTransactionFees.ETransactionType = db.ETransactionTypes.Where(i => i.TransactionType == "bill_payment").FirstOrDefault().Id;
                        mWalletTransactionFees.TransactionFee = transaction_fee;//record the actual fee not the link to the fee
                        db.MWalletTransactionFees.Add(mWalletTransactionFees);

                        db.SaveChanges();
                        menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/PayBill/successfully_paid_bill.txt");
                        menu = menu.Replace("{{data}}", payBillResponse.data.ToString());
                        WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();//send
                        WalletApi.Menus.MenuMain.Menu(message);//return client to the main menu

                    }
                    else////failed
                    {
                        menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                        menu = menu.Replace("{{error_message}}",
                            "Failed to complete transaction"
                            + Environment.NewLine
                            + payBillResponse.data + " "
                            + payBillResponse.msg
                           );
                        menu = menu.Replace("*Try again.*", "");
                        WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();//send
                        WalletApi.Menus.MenuMain.Menu(message);//return client to the main menu
                    }
                }
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, "error happended").Wait();//send
            }
            finally
            {
                db.Dispose();
            }
        }


    }
}
