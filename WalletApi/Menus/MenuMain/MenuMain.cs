﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WalletApi.Controllers;
using WalletApi.Models;

namespace WalletApi.Menus
{
    public class MenuMain
    {
        public static string Menu(RecieveMessageCallBack message)
        {
            var db = new dbContext();

            try
            {
                var menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/menu_main.txt");
                var wallet = db.MWallet.Find(message.from);
                var image_path = $"{Globals.base_path}/Raw/main_menu.png";
                var response = WhatsAppWalletApiController.SendMessageImageAndText(message.from, image_path, menu).Result;//send
                var session_data = db.MWalletSession.Find(message.from);
                session_data.Date = DateTime.Now;
                session_data.LastMenu = "main";
                session_data.IndexCounter = 0;//reset indexCounter
                session_data.Data = null;
                db.SaveChanges();//update session
                db.Dispose();
                return response.Value.ToString();
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
                return "";
            }
            finally
            {
                db.Dispose();
            }
        }

    }
}
