﻿using Microsoft.EntityFrameworkCore;
using Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WalletApi.Controllers;
using WalletApi.Models;

namespace WalletApi.Menus
{
    public class BuyAirtime
    {
        //index
        //prompt enter account and amount
        public static string Menu(RecieveMessageCallBack message)
        {
            try
            {
                var db = new dbContext();
                var menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/BuyAirtime/main_menu.txt");
                var response = WhatsAppWalletApiController.SendMessageText(message.from, menu).Result;
                var session_data = db.MWalletSession.Find(message.from);
                session_data.Date = DateTime.Now;
                session_data.LastMenu = "main.6";
                db.Entry(session_data).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();//update session
                db.Dispose();
                return response.Value.ToString();
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
                return "";
            }
        }

        /// <summary>
        /// accept amount and account
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static string Menu_1(RecieveMessageCallBack message)
        {
            var db = new dbContext();

            try
            {
                var my_wallet = db.MWallet.Find(message.from);

                var menu = string.Empty;

                //incorrect format objects must be 2
                if (message.content.Split('#').Length != 2)
                {
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", $"Payer details are not in the correct format");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();//send
                    //prev menu
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/BuyAirtime/main_menu.txt");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    return "failed";
                }

                //amount must be a number
                decimal amount_to_buy = 0;
                var account_number = message.content.Split('#')[0];
                var amount_entered = message.content.Split('#')[1];

                if (!decimal.TryParse(amount_entered, out amount_to_buy))
                {
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", $"Amount must be a number");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();//send
                    //prev menu
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/BuyAirtime/main_menu.txt");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    return "failed";
                }

                //amount must be more than 0
                if (amount_to_buy <= 0)
                {
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", $"Amount must be more than 0.");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();//send
                    //prev menu
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/BuyAirtime/main_menu.txt");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    return "failed";
                }

                //
                var transaction_fee = Helpers.GetTransactionFee("bill_payment", amount_to_buy);

                //balance must be sufficient
                if (my_wallet.Balance < (amount_to_buy + transaction_fee))
                {
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", "Insufficient balance to make this transaction");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();//send
                    db.Dispose();
                    return "failed";
                }
                else
                {
                    //save session information
                    if (account_number.StartsWith("071")) Helpers.WriteSessionData(message, "biller_code", "netone");
                    if (account_number.StartsWith("073")) Helpers.WriteSessionData(message, "biller_code", "telecel");
                    if (account_number.StartsWith("077")) Helpers.WriteSessionData(message, "biller_code", "econet");
                    if (account_number.StartsWith("078")) Helpers.WriteSessionData(message, "biller_code", "econet");
                    Helpers.WriteSessionData(message, "biller_category", "AIRTIME & DATA");
                    Helpers.WriteSessionData(message, "product_name", "airtime");
                    Helpers.WriteSessionData(message, "account_number", account_number);
                    Helpers.WriteSessionData(message, "product_id", "1");
                    Helpers.WriteSessionData(message, "qty", "1");//qty must be 1
                    Helpers.WriteSessionData(message, "address", "address");
                    Helpers.WriteSessionData(message, "unit_price", $"{amount_to_buy.ToString("0.00")}");//amount to buy
                    //
                    var session_data_data = Helpers.ReadSessionData(message);
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/BuyAirtime/display_confirm_purchase_and_request_pin.txt");
                    menu = menu.Replace("{{biller}}", session_data_data.biller_code.ToString());
                    menu = menu.Replace("{{payer_account_no}}", session_data_data.account_number.ToString());
                    menu = menu.Replace("{{total}}", session_data_data.unit_price.ToString());
                    //
                    var session_data = db.MWalletSession.Find(message.from);
                    session_data.Date = DateTime.Now;
                    session_data.LastMenu = "main.6.1";
                    db.SaveChanges();//update session
                    //
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();//send
                    return "ok";
                }

            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
                return "";
            }
            finally
            {
                db.Dispose();
            }
        }

        /// <summary>
        /// Accepts pin
        /// Validates pin
        /// Deducts the amount
        /// Saves the transaction
        /// </summary>
        /// <param name="message"></param>
        /// <param name="direction"></param>
        public static void ExecutePayment(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            try
            {
                var my_wallet = db.MWallet.Find(message.from);
                var menu = string.Empty;

                if (message.content != my_wallet.Pin)
                {
                    //invalid pin
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", "Incorrect pin");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();//send
                }
                else
                {
                    //pin is valid continue

                    ///* post bilerCart to the server
                    /// deduct the (amount + fees from client wallet)
                    /// 
                    var session_data_data = Helpers.ReadSessionData(message);
                    decimal qty = decimal.Parse(session_data_data.qty.ToString());
                    decimal price = decimal.Parse(session_data_data.unit_price.ToString());
                    var amount_to_pay = (qty * price);

                    //execute the paybill
                    var billerCartRequest = Helpers.CreateBillerCart(message);
                    string json_response = BillersController.ProcessPayment(billerCartRequest).Result;
                    dynamic payBillResponse = JsonConvert.DeserializeObject(json_response);

                    //if success
                    if (!payBillResponse.res.Equals("00"))
                    {
                        var transaction_fee = Helpers.GetTransactionFee("bill_payment", amount_to_pay);

                        //deduct the amount to cash out
                        my_wallet.Balance -= amount_to_pay;
                        //deduct the fee charged
                        my_wallet.Balance -= transaction_fee;

                        var biller_code = session_data_data.biller_code;

                        //record this transaction
                        var deduction_transaction = new MTransactions();
                        deduction_transaction.WhatsAppMobileNumber = message.from;
                        deduction_transaction.Date = DateTime.Now;
                        deduction_transaction.Amount = amount_to_pay;//dont record the fee here, only record the deduction amount
                        deduction_transaction.EnumTransactionType = db.ETransactionTypes.Where(i => i.TransactionType == "bill_payment").First().Id;
                        deduction_transaction.From = message.from;//from me to biller
                        deduction_transaction.To = biller_code;//reciever
                        db.MTransactions.Add(deduction_transaction);//save transaction

                        //record also the transaction fee into the db
                        var mWalletTransactionFees = new MWalletTransactionFees();
                        mWalletTransactionFees.Date = DateTime.Now;
                        mWalletTransactionFees.WhatsAppMobileNumber = message.from;
                        mWalletTransactionFees.ETransactionType = db.ETransactionTypes.Where(i => i.TransactionType == "bill_payment").FirstOrDefault().Id;
                        mWalletTransactionFees.TransactionFee = transaction_fee;//record the actual fee not the link to the fee
                        db.MWalletTransactionFees.Add(mWalletTransactionFees);

                        db.SaveChanges();
                        menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/PayBill/successfully_paid_bill.txt");

                        menu = menu.Replace("{{data}}", payBillResponse.data.ToString());
                        WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();//send
                        WalletApi.Menus.MenuMain.Menu(message);//return client to the main menu

                    }
                    else////failed
                    {
                        menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                        menu = menu.Replace("{{error_message}}", "Failed to complete transaction: " + payBillResponse.msg);
                        WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();//send
                        WalletApi.Menus.MenuMain.Menu(message);//return client to the main menu
                    }
                }
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, "error happended").Wait();//send
            }
            finally
            {
                db.Dispose();
            }
        }





    }
}
