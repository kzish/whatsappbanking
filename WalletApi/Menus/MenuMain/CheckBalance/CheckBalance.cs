﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WalletApi.Controllers;
using WalletApi.Models;

namespace WalletApi.Menus
{
    public class CheckBalance
    {
        public static string Menu(RecieveMessageCallBack message)
        {
            try
            {
                var db = new dbContext();
                var menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/CheckBalance/check_balance.txt");
                var wallet = db.MWallet.Find(message.from);
                menu = menu.Replace("{{date}}", DateTime.Now.ToString("MMM, d yyyy"));
                menu = menu.Replace("{{balance}}", wallet.Balance.ToString("0.00"));
                var response = WhatsAppWalletApiController.SendMessageText(message.from, menu).Result;

                
                var session_data = db.MWalletSession.Find(message.from);
                session_data.Date = DateTime.Now;
                session_data.LastMenu = "main";
                db.Entry(session_data).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();//update session
                db.Dispose();

                return response.Value.ToString();
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
                return "";
            }
        }

    }
}
