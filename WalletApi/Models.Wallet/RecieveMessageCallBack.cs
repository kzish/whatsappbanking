﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Models
{
    /// <summary>
    /// a standard callback that is generated when an incoming message has been received by the platform
    /// app user sends a message to the platform
    /// </summary>
    public class RecieveMessageCallBack
    {
        public string integrationId { get; set; }
        public string integrationName { get; set; }
        public string content { get; set; }
        public string messageId { get; set; }
        public string relatedMessageId { get; set; }//contains id of replied to message
        public string from { get; set; }
        public string timestamp { get; set; }
    }
}
