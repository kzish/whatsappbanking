﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WalletApi.Models.Wallet
{
    public class GupShupRecieveMessageCallBack
    {
        public string app { get; set; }
        public string timestamp { get; set; }
        public string version { get; set; }
        public string type { get; set; }
        public Payload payload { get; set; } = new Payload();
        public Sender sender { get; set; } = new Sender();
    }

    public class Sender
    {
        public string phone { get; set; }
        public string name { get; set; }
    }

    public class Payload
    {
        public string id { get; set; }
        public string source { get; set; }
        public string type { get; set; }
        public PayloadPayload payload { get; set; } = new PayloadPayload();
    }

    public class PayloadPayload
    {
        public string text { get; set; }
    }

}