﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Models
{
    /// <summary>
    ///  standard callback when the message has been read by recipient
    ///  read reciepts
    /// </summary>
    public class StatusCallback
    {
        public string integrationId { get; set; }
        public string integrationName { get; set; }
        public string timestamp { get; set; }
        public string statusCode { get; set; }
        public string status { get; set; }
        public string messageId { get; set; }
        public string clientMessageId { get; set; }
    }
}
