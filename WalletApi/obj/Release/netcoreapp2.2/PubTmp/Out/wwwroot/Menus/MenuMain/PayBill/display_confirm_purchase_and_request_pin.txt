﻿*WhatsAppBanking*

*💸✅Pay bill*

👮 Confirm payment

🔰 biller.......... {{biller}}
🔰 item.......... {{product_name}}
🔰 account... {{payer_account_no}}
🔰 qty............ {{qty}}
🔰 price......... {{price}}
🔰 address.... {{address}}
💲 total............ {{total}}

Enter pin to confirm payment.
_remember to 🚮delete the message with the pin._

_press #⃣  to return to main menu._