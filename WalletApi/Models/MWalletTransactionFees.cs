﻿using System;
using System.Collections.Generic;

namespace WalletApi.Models
{
    public partial class MWalletTransactionFees
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string WhatsAppMobileNumber { get; set; }
        public int ETransactionType { get; set; }
        public decimal TransactionFee { get; set; }

        public virtual MWallet WhatsAppMobileNumberNavigation { get; set; }
    }
}
