﻿using System;
using System.Collections.Generic;

namespace WalletApi.Models
{
    public partial class MWalletSession
    {
        public string WhatsAppMobileNumber { get; set; }
        public string LastMenu { get; set; }
        public DateTime Date { get; set; }
        public int IndexCounter { get; set; }
        public string Data { get; set; }
    }
}
