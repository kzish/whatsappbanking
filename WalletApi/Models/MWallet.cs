﻿using System;
using System.Collections.Generic;

namespace WalletApi.Models
{
    public partial class MWallet
    {
        public MWallet()
        {
            MWalletTransactionFees = new HashSet<MWalletTransactionFees>();
        }

        public string WhatsAppMobileNumber { get; set; }
        public decimal Balance { get; set; }
        public DateTime DateCreated { get; set; }
        public string Pin { get; set; }

        public virtual ICollection<MWalletTransactionFees> MWalletTransactionFees { get; set; }
    }
}
