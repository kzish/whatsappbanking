﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WalletApi.Models
{
    public partial class dbContext : DbContext
    {
        public dbContext()
        {
        }

        public dbContext(DbContextOptions<dbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ETransactionTypes> ETransactionTypes { get; set; }
        public virtual DbSet<MOtpTokens> MOtpTokens { get; set; }
        public virtual DbSet<MTransactionCharges> MTransactionCharges { get; set; }
        public virtual DbSet<MTransactions> MTransactions { get; set; }
        public virtual DbSet<MWallet> MWallet { get; set; }
        public virtual DbSet<MWalletAgents> MWalletAgents { get; set; }
        public virtual DbSet<MWalletSession> MWalletSession { get; set; }
        public virtual DbSet<MWalletTransactionFees> MWalletTransactionFees { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("server=localhost;database=WhatsAppBanking;Trusted_Connection=true;");
                //optionsBuilder.UseSqlServer("Server=WIN-VJK2LL0LG4U\\SQLEXPRESS;database=WhatsAppBanking;User Id=sa;Password=gZi3hg8dn210Q;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<ETransactionTypes>(entity =>
            {
                entity.ToTable("e_transaction_types");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.MaximumLimit)
                    .HasColumnName("maximum_limit")
                    .HasColumnType("money");

                entity.Property(e => e.TransactionType)
                    .IsRequired()
                    .HasColumnName("transaction_type")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MOtpTokens>(entity =>
            {
                entity.HasKey(e => e.WhatsAppMobileNumber);

                entity.ToTable("m_otp_tokens");

                entity.Property(e => e.WhatsAppMobileNumber)
                    .HasColumnName("whats_app_mobile_number")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.DateCreated)
                    .HasColumnName("date_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.ExpirationDate)
                    .HasColumnName("expiration_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Otp)
                    .IsRequired()
                    .HasColumnName("otp")
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MTransactionCharges>(entity =>
            {
                entity.ToTable("m_transaction_charges");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ETransactionTypeId).HasColumnName("e_transaction_type_id");

                entity.Property(e => e.IsPercentage).HasColumnName("is_percentage");

                entity.Property(e => e.LowerLimit)
                    .HasColumnName("lower_limit")
                    .HasColumnType("money");

                entity.Property(e => e.TransactionFee)
                    .HasColumnName("transaction_fee")
                    .HasColumnType("money");

                entity.Property(e => e.UpperLimit)
                    .HasColumnName("upper_limit")
                    .HasColumnType("money");

                entity.HasOne(d => d.ETransactionType)
                    .WithMany(p => p.MTransactionCharges)
                    .HasForeignKey(d => d.ETransactionTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_m_transaction_charges_e_transaction_types");
            });

            modelBuilder.Entity<MTransactions>(entity =>
            {
                entity.ToTable("m_transactions");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Amount)
                    .HasColumnName("amount")
                    .HasColumnType("money");

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("datetime");

                entity.Property(e => e.EnumTransactionType).HasColumnName("enum_transaction_type");

                entity.Property(e => e.From)
                    .IsRequired()
                    .HasColumnName("from")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.To)
                    .IsRequired()
                    .HasColumnName("to")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.WhatsAppMobileNumber)
                    .IsRequired()
                    .HasColumnName("whats_app_mobile_number")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MWallet>(entity =>
            {
                entity.HasKey(e => e.WhatsAppMobileNumber);

                entity.ToTable("m_wallet");

                entity.Property(e => e.WhatsAppMobileNumber)
                    .HasColumnName("whats_app_mobile_number")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Balance)
                    .HasColumnName("balance")
                    .HasColumnType("money");

                entity.Property(e => e.DateCreated)
                    .HasColumnName("date_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.Pin)
                    .IsRequired()
                    .HasColumnName("pin")
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MWalletAgents>(entity =>
            {
                entity.HasKey(e => e.AgentCode);

                entity.ToTable("m_wallet_agents");

                entity.Property(e => e.AgentCode)
                    .HasColumnName("agent_code")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.AgentAddress)
                    .IsRequired()
                    .HasColumnName("agent_address")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AgentCity)
                    .IsRequired()
                    .HasColumnName("agent_city")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AgentName)
                    .IsRequired()
                    .HasColumnName("agent_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AgentTown)
                    .IsRequired()
                    .HasColumnName("agent_town")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MWalletSession>(entity =>
            {
                entity.HasKey(e => e.WhatsAppMobileNumber);

                entity.ToTable("m_wallet_session");

                entity.Property(e => e.WhatsAppMobileNumber)
                    .HasColumnName("whats_app_mobile_number")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Data)
                    .HasColumnName("data")
                    .IsUnicode(false);

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("datetime");

                entity.Property(e => e.IndexCounter).HasColumnName("index_counter");

                entity.Property(e => e.LastMenu)
                    .IsRequired()
                    .HasColumnName("last_menu")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MWalletTransactionFees>(entity =>
            {
                entity.ToTable("m_wallet_transaction_fees");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("datetime");

                entity.Property(e => e.ETransactionType).HasColumnName("e_transaction_type");

                entity.Property(e => e.TransactionFee)
                    .HasColumnName("transaction_fee")
                    .HasColumnType("money");

                entity.Property(e => e.WhatsAppMobileNumber)
                    .IsRequired()
                    .HasColumnName("whats_app_mobile_number")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.WhatsAppMobileNumberNavigation)
                    .WithMany(p => p.MWalletTransactionFees)
                    .HasForeignKey(d => d.WhatsAppMobileNumber)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_m_wallet_transaction_fees_m_wallet");
            });
        }
    }
}
