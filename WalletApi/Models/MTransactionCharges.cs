﻿using System;
using System.Collections.Generic;

namespace WalletApi.Models
{
    public partial class MTransactionCharges
    {
        public int Id { get; set; }
        public int ETransactionTypeId { get; set; }
        public decimal LowerLimit { get; set; }
        public decimal UpperLimit { get; set; }
        public decimal TransactionFee { get; set; }
        public bool IsPercentage { get; set; }

        public virtual ETransactionTypes ETransactionType { get; set; }
    }
}
