﻿using System;
using System.Collections.Generic;

namespace WalletApi.Models
{
    public partial class MOtpTokens
    {
        public string WhatsAppMobileNumber { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime ExpirationDate { get; set; }
        public string Otp { get; set; }
    }
}
