﻿using System;
using System.Collections.Generic;

namespace WalletApi.Models
{
    public partial class ETransactionTypes
    {
        public ETransactionTypes()
        {
            MTransactionCharges = new HashSet<MTransactionCharges>();
        }

        public int Id { get; set; }
        public string TransactionType { get; set; }
        public decimal? MaximumLimit { get; set; }

        public virtual ICollection<MTransactionCharges> MTransactionCharges { get; set; }
    }
}
