﻿using System;
using System.Collections.Generic;

namespace WalletApi.Models
{
    public partial class MTransactions
    {
        public int Id { get; set; }
        public string WhatsAppMobileNumber { get; set; }
        public DateTime Date { get; set; }
        public decimal Amount { get; set; }
        public int EnumTransactionType { get; set; }
        public string From { get; set; }
        public string To { get; set; }
    }
}
