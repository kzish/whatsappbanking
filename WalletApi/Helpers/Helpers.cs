﻿using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.EntityFrameworkCore;
using Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WalletApi.Models;
using WalletApi.Models.Wallet;

public class Helpers
{
    public static HostingEnvironment host;

    public static string ImageToBase64(string path)
    {
        var base64String = string.Empty;
        using (Image image = Image.FromFile(path))
        {
            using (MemoryStream ms = new MemoryStream())
            {
                image.Save(ms, image.RawFormat);
                byte[] imageBytes = ms.ToArray();
                base64String = Convert.ToBase64String(imageBytes);
                ms.Dispose();
            }
            image.Dispose();
        }
        return base64String;
    }

    public static string GenerateAndSendOtp(string whatsapp_mobile_number)
    {
        Random generator = new Random();
        String r = generator.Next(0, 999999).ToString("D5");
        //save otp to db
        var db = new dbContext();
        //remove old otp
        var old_otp = db.MOtpTokens.Where(i => i.WhatsAppMobileNumber == whatsapp_mobile_number).FirstOrDefault();
        if (old_otp != null) db.MOtpTokens.Remove(old_otp);
        var otp = new MOtpTokens();
        otp.WhatsAppMobileNumber = whatsapp_mobile_number;
        otp.DateCreated = DateTime.Now;
        otp.ExpirationDate = DateTime.Now.AddMinutes(10);//expires after 10 minutes
        otp.Otp = r;
        db.MOtpTokens.Add(otp);
        db.SaveChanges();
        db.Dispose();
        //send the otp via sms
        return r;
    }


    public static decimal GetTransactionFee(string transaction_type, decimal transaction_amount)
    {
        var db = new dbContext();

        //charges
        var transaction_charge = db.ETransactionTypes
            .Where(i => i.TransactionType == transaction_type)
            .Include(i => i.MTransactionCharges)
            .FirstOrDefault();

        //this is the actual fee to be charged to this transaction
        decimal transaction_fee = 0;

        //compute the transaction fee
        foreach (var trans_charge in transaction_charge.MTransactionCharges)
        {
            //lies within the transaction band
            if (transaction_amount >= trans_charge.LowerLimit && transaction_amount <= trans_charge.UpperLimit)
            {
                //set the fee as a percentage of the deduction amount
                if (trans_charge.IsPercentage)
                {
                    transaction_fee = ((trans_charge.TransactionFee / 100) * transaction_amount);
                }
                else//set the fee as a flat fee
                {
                    transaction_fee = trans_charge.TransactionFee;
                }
                //we are done
                break;
            }
        }
        db.Dispose();
        return transaction_fee;
    }

    /// <summary>
    /// create the biller cart and populate the fields according to the 
    /// requirements of that specific biller at the api
    /// </summary>
    /// <param name="message"></param>
    /// <returns></returns>
    public static mBillerCartRequest CreateBillerCart(RecieveMessageCallBack message)
    {
        var db = new dbContext();
        var billerCart = new mBillerCartRequest();
       
        try
         {
            var session_data_data = Helpers.ReadSessionData(message);
            //
            decimal qty = decimal.Parse(session_data_data.qty.ToString());
            decimal unit_price = decimal.Parse(session_data_data.unit_price.ToString());
            string address = session_data_data.address.ToString();
            string product_id = session_data_data.product_id.ToString();
            string biller_code = session_data_data.biller_code.ToString();
            string account_number = session_data_data.account_number.ToString();

            //
            billerCart.biller_code = biller_code;
            billerCart.payment_ref = Guid.NewGuid().ToString();//concatenate with the time stamp
            billerCart.payer_name = message.from;
            billerCart.payer_mobile = message.from;
            billerCart.payer_details1 = "";
            billerCart.payer_details2 = "";
            billerCart.payer_details3 = "";
            billerCart.bank_code = "0000";//rubiem whatsapp banking bank code
            //
            billerCart.items = new List<mCartProduct>()
            {
                //only purchasing one item at a time
                new mCartProduct (){
                    product_code=product_id,
                    quantity=qty,
                    unit_price=unit_price
                }
            };

            //normalize for each biller
            if (biller_code == "fresh_in_a_box")
            {
                billerCart.payer_details1 = address;//delivery address
                billerCart.payer_details2 = message.from;//name
                billerCart.payer_details3 = message.from;//city
            }

            if (biller_code == "cimas")
            {
                billerCart.payer_details1 = "CIMAS";
                billerCart.payer_details2 = account_number;//account number
                billerCart.payer_details3 = string.Empty;
            }

            if (biller_code == "zesa_pre_paid")
            {
                billerCart.payer_details1 = "ZETDC_PPE";
                billerCart.payer_details2 = account_number;//account number
                billerCart.payer_details3 = string.Empty;
            }

            if (biller_code == "spca")
            {
                billerCart.payer_details1 = "SPCA";
                billerCart.payer_details2 = account_number;//account number
                billerCart.payer_details3 = string.Empty;
            }

            if (biller_code == "zinara")
            {
                billerCart.payer_details1 = "ICECASH";
                billerCart.payer_details2 = account_number;//account number
                billerCart.payer_details3 = string.Empty;
            }

            if (biller_code == "telone_broad_band")
            {
                billerCart.payer_details1 = "telone_bill";
                billerCart.payer_details2 = account_number;//account number
                billerCart.payer_details3 = string.Empty;
            }

            if (biller_code == "telone_bill")
            {
                billerCart.payer_details1 = account_number;//phone number (landline)
                billerCart.payer_details2 = string.Empty;
                billerCart.payer_details3 = string.Empty;
            }

            if (biller_code == "zuva")
            {
                billerCart.payer_details1 = "ZUVA";
                billerCart.payer_details2 = account_number;//account
                billerCart.payer_details3 = string.Empty;
            }

            if (biller_code == "zinwa")
            {
                billerCart.payer_details1 = "ZINWA";
                billerCart.payer_details2 = account_number;//account
                billerCart.payer_details3 = string.Empty;
            }

            if (biller_code == "netone")
            {
                billerCart.payer_details1 = "NETONE";
                billerCart.payer_details2 = account_number;//account
                billerCart.payer_details3 = string.Empty;
            }

            if (biller_code == "econet")
            {
                billerCart.payer_details1 = "ECONET";
                billerCart.payer_details2 = account_number;//account
                billerCart.payer_details3 = string.Empty;
            }

            if (biller_code == "telecel")
            {
                billerCart.payer_details1 = "TELECEL";
                billerCart.payer_details2 = account_number;//account
                billerCart.payer_details3 = string.Empty;
            }

            if (biller_code == "dstv")
            {
                billerCart.payer_details1 = "DSTV";
                billerCart.payer_details2 = account_number;//account
                billerCart.payer_details3 = "TOPUP";//type
            }

            if (biller_code == "bulawayo")
            {
                billerCart.payer_details1 = "BULAW";
                billerCart.payer_details2 = account_number;//account
                billerCart.payer_details3 = string.Empty;
            }


            if (biller_code == "zol")
            {
                billerCart.payer_details1 = "ZOL";
                billerCart.payer_details2 = account_number;//account
                billerCart.payer_details3 = string.Empty;
            }

            if (biller_code == "ctrade")
            {
                billerCart.payer_details1 = string.Empty;
                billerCart.payer_details2 = string.Empty;
                billerCart.payer_details3 = string.Empty;
            }


            return billerCart;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            db.Dispose();
        }
    }


    /// <summary>
    /// return a dynamic object of the session_data.Data
    /// </summary>
    /// <param name="message"></param>
    /// <returns></returns>
    public static dynamic ReadSessionData(RecieveMessageCallBack message)
    {
        var db = new dbContext();
        try
        {
            var session_data = db.MWalletSession.Find(message.from);
            dynamic data = JsonConvert.DeserializeObject(session_data.Data);
            return data;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            db.Dispose();
        }
    }

    /// <summary>
    /// inserts or updates a key value pair into the session_data.Data
    /// </summary>
    /// <param name="message"></param>
    /// <param name="key"></param>
    /// <param name="value"></param>
    /// <returns></returns>
    public static bool WriteSessionData(RecieveMessageCallBack message, string key, string value)
    {
        var db = new dbContext();
        try
        {
            var session_data = db.MWalletSession.Find(message.from);
            dynamic data = null;
            //
            if (!string.IsNullOrEmpty(session_data.Data))
                data = JsonConvert.DeserializeObject(session_data.Data);//get the json data
            else
                data = new JObject();
            //
            data[key] = value;//add the key and value
            session_data.Data = JsonConvert.SerializeObject(data);//convert back to string
            db.Entry(session_data).State = EntityState.Modified;//save the session entry
            db.SaveChanges();
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
        finally
        {
            db.Dispose();
        }
    }

    //
    public static decimal ecocash_minimum_topup_amount = 10;
    public static bool ExecuteEcocashPayment(string ecocash_phone_number,decimal topup_amount)
    {
        return true;
       try
        {
            //execute top up request
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
            var string_content_string = @"{
                                                ""username"": ""scb"",
                                                ""password"": ""a!hksrtsb""
                                            }";
            var stringContent = new StringContent(string_content_string, Encoding.UTF8, "application/json");

            string json = httpClient.PostAsync($"{Globals.smartpay_end_point}/api/users/authenticate", stringContent)
                .Result
                .Content
                .ReadAsStringAsync()
                .Result;
            dynamic json_result = JsonConvert.DeserializeObject(json);

            string auth_token = json_result.token;
            httpClient.DefaultRequestHeaders.Clear();
            httpClient.DefaultRequestHeaders.Add("Authorization", $"Bearer {auth_token}");

            var response = httpClient.GetAsync($"{Globals.smartpay_end_point}/api/ecocash/pay?IntergrationId=1&phonenumber={ecocash_phone_number}&reference={Guid.NewGuid().ToString()}&amount={topup_amount}")
                .Result;


            var json_response_string = response
                .Content
                .ReadAsStringAsync()
                .Result;

            dynamic jresponse = JsonConvert.DeserializeObject(json_response_string);
            string StatusCode = jresponse.StatusCode;
            if (StatusCode == "1") return true;
            else return false;
        }
        catch(Exception x)

        {
            return false;
        }
    }


}
