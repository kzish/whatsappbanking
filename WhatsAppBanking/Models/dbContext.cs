﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WhatsAppBanking.Models
{
    public partial class dbContext : DbContext
    {
        public dbContext()
        {
        }

        public dbContext(DbContextOptions<dbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<MOtpTokens> MOtpTokens { get; set; }
        public virtual DbSet<MWallet> MWallet { get; set; }
        public virtual DbSet<MWalletSession> MWalletSession { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("server=localhost;database=WhatsAppBanking;Trusted_Connection=true;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<MOtpTokens>(entity =>
            {
                entity.HasKey(e => e.WhatsAppMobileNumber);

                entity.ToTable("m_otp_tokens");

                entity.Property(e => e.WhatsAppMobileNumber)
                    .HasColumnName("whats_app_mobile_number")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.DateCreated)
                    .HasColumnName("date_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.ExpirationDate)
                    .HasColumnName("expiration_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Otp)
                    .IsRequired()
                    .HasColumnName("otp")
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MWallet>(entity =>
            {
                entity.HasKey(e => e.WhatsAppMobileNumber);

                entity.ToTable("m_wallet");

                entity.Property(e => e.WhatsAppMobileNumber)
                    .HasColumnName("whats_app_mobile_number")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Balance)
                    .HasColumnName("balance")
                    .HasColumnType("money");
            });

            modelBuilder.Entity<MWalletSession>(entity =>
            {
                entity.HasKey(e => e.WhatsAppMobileNumber);

                entity.ToTable("m_wallet_session");

                entity.Property(e => e.WhatsAppMobileNumber)
                    .HasColumnName("whats_app_mobile_number")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("datetime");

                entity.Property(e => e.LastMenu)
                    .IsRequired()
                    .HasColumnName("last_menu")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });
        }
    }
}
