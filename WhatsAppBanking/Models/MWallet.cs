﻿using System;
using System.Collections.Generic;

namespace WhatsAppBanking.Models
{
    public partial class MWallet
    {
        public string WhatsAppMobileNumber { get; set; }
        public decimal Balance { get; set; }
    }
}
