﻿using System;
using System.Collections.Generic;

namespace WhatsAppBanking.Models
{
    public partial class MWalletSession
    {
        public string WhatsAppMobileNumber { get; set; }
        public string LastMenu { get; set; }
        public DateTime Date { get; set; }
    }
}
