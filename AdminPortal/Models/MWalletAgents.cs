﻿using System;
using System.Collections.Generic;

namespace AdminPortal.Models
{
    public partial class MWalletAgents
    {
        public string AgentCode { get; set; }
        public string AgentName { get; set; }
        public string AgentCity { get; set; }
        public string AgentTown { get; set; }
        public string AgentAddress { get; set; }
    }
}
