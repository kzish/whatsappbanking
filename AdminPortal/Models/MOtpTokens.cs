﻿using System;
using System.Collections.Generic;

namespace AdminPortal.Models
{
    public partial class MOtpTokens
    {
        public string WhatsAppMobileNumber { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime ExpirationDate { get; set; }
        public string Otp { get; set; }

        public virtual MWallet WhatsAppMobileNumberNavigation { get; set; }
    }
}
