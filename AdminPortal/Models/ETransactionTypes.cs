﻿using System;
using System.Collections.Generic;

namespace AdminPortal.Models
{
    public partial class ETransactionTypes
    {
        public ETransactionTypes()
        {
            MTransactionCharges = new HashSet<MTransactionCharges>();
            MTransactions = new HashSet<MTransactions>();
            MWalletTransactionFees = new HashSet<MWalletTransactionFees>();
        }

        public int Id { get; set; }
        public string TransactionType { get; set; }
        public decimal? MaximumLimit { get; set; }

        public virtual ICollection<MTransactionCharges> MTransactionCharges { get; set; }
        public virtual ICollection<MTransactions> MTransactions { get; set; }
        public virtual ICollection<MWalletTransactionFees> MWalletTransactionFees { get; set; }
    }
}
