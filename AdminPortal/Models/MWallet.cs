﻿using System;
using System.Collections.Generic;

namespace AdminPortal.Models
{
    public partial class MWallet
    {
        public MWallet()
        {
            MTransactions = new HashSet<MTransactions>();
            MWalletTransactionFees = new HashSet<MWalletTransactionFees>();
        }

        public string WhatsAppMobileNumber { get; set; }
        public decimal Balance { get; set; }
        public DateTime DateCreated { get; set; }
        public string Pin { get; set; }

        public virtual MOtpTokens MOtpTokens { get; set; }
        public virtual ICollection<MTransactions> MTransactions { get; set; }
        public virtual ICollection<MWalletTransactionFees> MWalletTransactionFees { get; set; }
    }
}
