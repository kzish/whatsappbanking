#pragma checksum "C:\Users\samaritan\Documents\Visual Studio 2017\projects\rubiem\WhatsAppBanking\AdminPortal\Views\TransactionTypes\TransactionFees.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "e6fd111fabb19c8a97dbf667b0d807d447c58f02"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_TransactionTypes_TransactionFees), @"mvc.1.0.view", @"/Views/TransactionTypes/TransactionFees.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/TransactionTypes/TransactionFees.cshtml", typeof(AspNetCore.Views_TransactionTypes_TransactionFees))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"e6fd111fabb19c8a97dbf667b0d807d447c58f02", @"/Views/TransactionTypes/TransactionFees.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"57218c316b6921e2cd61027a2387edc31a2d9471", @"/Views/_ViewImports.cshtml")]
    public class Views_TransactionTypes_TransactionFees : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/assets/img/_loading.gif"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("action", new global::Microsoft.AspNetCore.Html.HtmlString("~/TransactionTypes/AddFeeCharge"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", new global::Microsoft.AspNetCore.Html.HtmlString("post"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 1 "C:\Users\samaritan\Documents\Visual Studio 2017\projects\rubiem\WhatsAppBanking\AdminPortal\Views\TransactionTypes\TransactionFees.cshtml"
  
    Layout = "~/Views/Shared/_LayoutMain.cshtml";
    //load the charges via ajax
    var transaction_type_id = (int)ViewBag.transaction_type_id;

#line default
#line hidden
            BeginContext(156, 407, true);
            WriteLiteral(@"

<div class=""container"">
    <div class=""row"">
        <div class=""col-md-12"">
            <a href=""javascript.void(0);"" onclick=""AddFee(); return false;"" style=""color:blue;"">
                &nbsp;&nbsp;&nbsp;&nbsp; Add Fee
            </a>
        </div>
    </div>
    <br />
    <div class=""row"">
        <div class=""col-md-12"" id=""data"">
            <center id=""loader"">
                ");
            EndContext();
            BeginContext(563, 39, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "e6fd111fabb19c8a97dbf667b0d807d447c58f024936", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(602, 562, true);
            WriteLiteral(@"
            </center>
        </div>
    </div>
</div>


<div class=""modal"" tabindex=""-1"" role=""dialog"" id=""new_fee"">
    <div class=""modal-dialog"" role=""document"">
        <div class=""modal-content"">
            <div class=""modal-header"">
                <h5 class=""modal-title"">Add Fee</h5>
                <button type=""button"" class=""close"" data-dismiss=""modal"" aria-label=""Close"">
                    <span aria-hidden=""true"">&times;</span>
                </button>
            </div>
            <div class=""modal-body"">
                ");
            EndContext();
            BeginContext(1164, 2507, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "e6fd111fabb19c8a97dbf667b0d807d447c58f026698", async() => {
                BeginContext(1225, 42, true);
                WriteLiteral("\r\n                    <input type=\"hidden\"");
                EndContext();
                BeginWriteAttribute("value", " value=\"", 1267, "\"", 1295, 1);
#line 38 "C:\Users\samaritan\Documents\Visual Studio 2017\projects\rubiem\WhatsAppBanking\AdminPortal\Views\TransactionTypes\TransactionFees.cshtml"
WriteAttributeValue("", 1275, transaction_type_id, 1275, 20, false);

#line default
#line hidden
                EndWriteAttribute();
                BeginContext(1296, 2368, true);
                WriteLiteral(@" name=""ETransactionTypeId"" />

                    <div class=""row"">
                        <div class=""col-md-12"">
                            <div class=""form-group"">
                                <input type=""number"" name=""LowerLimit"" step=""any"" class=""form-control"" placeholder=""0.00"" required>
                                <small class=""form-text text-muted""><span style=""color:red;"">* </span>Lower Limit</small>
                            </div>
                        </div>
                    </div>

                    <div class=""row"">
                        <div class=""col-md-12"">
                            <div class=""form-group"">
                                <input type=""number"" name=""UpperLimit"" step=""any"" class=""form-control"" placeholder=""0.00"" required>
                                <small class=""form-text text-muted""><span style=""color:red;"">* </span>Upper Limit</small>
                            </div>
                        </div>
                    </div>
");
                WriteLiteral(@"
                    <div class=""row"">
                        <div class=""col-md-12"">
                            <div class=""form-group"">
                                <input type=""number"" name=""TransactionFee"" step=""any"" class=""form-control"" placeholder=""0.00"" required>
                                <small class=""form-text text-muted""><span style=""color:red;"">* </span>Transaction Fee</small>
                            </div>
                        </div>
                    </div>

                    <div class=""row"">
                        <div class=""col-md-12"">
                            <div class=""form-group"">
                                <select name=""IsPercentage"" class=""form-control"">
                                    <option value=""false"">No</option>
                                    <option value=""true"">Yes</option>
                                </select>
                                <small class=""form-text text-muted""><span style=""color:red;"">* </span>Is Perc");
                WriteLiteral(@"entage</small>
                            </div>
                        </div>
                    </div>



                    <button type=""submit"" class=""btn btn-primary"">Save</button>
                    <button type=""button"" class=""btn btn-secondary"" data-dismiss=""modal"">Close</button>
                ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(3671, 124, true);
            WriteLiteral("\r\n            </div>\r\n\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n\r\n<script>\r\n    $.ajax({\r\n        type: \"GET\",\r\n        url: \"");
            EndContext();
            BeginContext(3797, 53, false);
#line 94 "C:\Users\samaritan\Documents\Visual Studio 2017\projects\rubiem\WhatsAppBanking\AdminPortal\Views\TransactionTypes\TransactionFees.cshtml"
          Write(Url.Content("~/TransactionTypes/ajaxTransactionFees"));

#line default
#line hidden
            EndContext();
            BeginContext(3851, 1, true);
            WriteLiteral("/");
            EndContext();
            BeginContext(3853, 19, false);
#line 94 "C:\Users\samaritan\Documents\Visual Studio 2017\projects\rubiem\WhatsAppBanking\AdminPortal\Views\TransactionTypes\TransactionFees.cshtml"
                                                                  Write(transaction_type_id);

#line default
#line hidden
            EndContext();
            BeginContext(3872, 390, true);
            WriteLiteral(@""",
        success: function (data) {
            $(""#data"").html(data);
            $(""#loader"").hide();
        },
        error: function () {
            alert('Server error');
            $(""#loader"").hide();
        },
    }).then(function () {
         $(""#loader"").hide();
    });



    function AddFee() {
        $(""#new_fee"").modal(""show"");
    }

</script>
");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
