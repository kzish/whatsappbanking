﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdminPortal.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace AdminPortal.Controllers
{
    [Route("Wallets")]
    public class WalletsController : Controller
    {
        dbContext db = new dbContext();

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }

        [HttpGet("ajaxAllWallets")]
        public IActionResult ajaxAllWallets()
        {
            var wallets = db.MWallet
                .Include(i => i.MOtpTokens)
                .ToList();
            ViewBag.wallets = wallets;
            return View();
        }


        [HttpGet("AllWallets")]
        public IActionResult AllWallets()
        {
            ViewBag.title = "Wallets";
            return View();
        }


        [HttpGet("ajaxAllTransactions/{whats_app_mobile_number}")]
        public IActionResult ajaxAllTransactions(string whats_app_mobile_number)
        {
            var transactions = db.MTransactions
                .Where(i => i.WhatsAppMobileNumber == whats_app_mobile_number)
                .Include(i => i.EnumTransactionTypeNavigation)
                .ToList();
            ViewBag.transactions = transactions;
            return View();
        }


        [HttpGet("AllTransactions/{whats_app_mobile_number}")]
        public IActionResult AllTransactions(string whats_app_mobile_number)
        {
            ViewBag.title = "Transactions";
            var wallet = db.MWallet.Find(whats_app_mobile_number);
            ViewBag.wallet = wallet;
            return View();
        }

    }
}
