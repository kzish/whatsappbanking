﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdminPortal.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;


/// <summary>
/// transaction charges
/// </summary>
namespace AdminPortal.Controllers
{
    [Route("TransactionTypes")]
    public class TransactionTypesController : Controller
    {
        dbContext db = new dbContext();


        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }

        [HttpGet("ajaxAllTransactionTypes")]
        public IActionResult ajaxAllTransactionTypes()
        {
            var types = db.ETransactionTypes.ToList();
            ViewBag.types = types;
            return View();
        }


        [HttpGet("AllTransactionTypes")]
        public IActionResult AllTransactionTypes()
        {
            ViewBag.title = "TransactionTypes";
            return View();
        }


        [HttpGet("ajaxTransactionFees/{transaction_type_id}")]
        public IActionResult ajaxTransactionFees(int transaction_type_id)
        {
            var fees = db.MTransactionCharges
                .Include(i => i.ETransactionType)
                .Where(i => i.ETransactionTypeId == transaction_type_id)
                .ToList();
            ViewBag.fees = fees;
            return View();
        }


        [HttpGet("TransactionFees/{transaction_type_id}")]
        public IActionResult TransactionFees(int transaction_type_id)
        {
            ViewBag.title = "TransactionFees";
            ViewBag.transaction_type_id = transaction_type_id;
            return View();
        }

        [HttpPost("AddFeeCharge")]
        public IActionResult AddFeeCharge(MTransactionCharges fee_charge)
        {
            try
            {
                var over_lapping_fee_lower_limit = db.MTransactionCharges
                    .Where(i => i.ETransactionTypeId == fee_charge.ETransactionTypeId)//this type
                    .Where(i => i.LowerLimit <= fee_charge.LowerLimit)//this lower limit is within lower and upper of existing
                    .Where(i => i.UpperLimit >= fee_charge.LowerLimit)
                    .Any();

                var over_lapping_fee_upper_limit = db.MTransactionCharges
                   .Where(i => i.ETransactionTypeId == fee_charge.ETransactionTypeId)//this type
                   .Where(i => i.LowerLimit <= fee_charge.UpperLimit)//this upper limit is within lower and upper of existing
                   .Where(i => i.UpperLimit >= fee_charge.UpperLimit)
                   .Any();

                if (over_lapping_fee_lower_limit || over_lapping_fee_upper_limit)
                {
                    TempData["type"] = "error";
                    TempData["msg"] = "Overlapping fee charges";
                }
                else
                {
                    db.MTransactionCharges.Add(fee_charge);
                    db.SaveChanges();
                    TempData["type"] = "success";
                    TempData["msg"] = "Saved";
                }

                return RedirectToAction("TransactionFees"
                    , "TransactionTypes"
                    , new { transaction_type_id = fee_charge.ETransactionTypeId });
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
                return RedirectToAction("TransactionFees"
                    , "TransactionTypes"
                    , new { transaction_type_id = fee_charge.ETransactionTypeId });
            }
        }




        [HttpPost("DeleteFeeCharge")]
        public IActionResult DeleteFeeCharge(int id)
        {
            var fee_charge = db.MTransactionCharges.Find(id);

            try
            {
                db.MTransactionCharges.Remove(fee_charge);
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Removed";
                return RedirectToAction("TransactionFees"
                    , "TransactionTypes"
                    , new { transaction_type_id = fee_charge.ETransactionTypeId });
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
                return RedirectToAction("TransactionFees"
                    , "TransactionTypes"
                    , new { transaction_type_id = fee_charge.ETransactionTypeId });
            }
        }



    }
}
