﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdminPortal.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace AdminPortal.Controllers
{



    [Route("Home")]
    [Route("")]
    [Authorize(Roles = "admin")]
    public class HomeController : Controller
    {
        dbContext db = new dbContext();

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }

        [Route("Dashboard")]
        [Route("")]
        public IActionResult Dashboard(string date_from, string date_to)
        {
            ViewBag.title = "Dashboard";
            var chart1 = string.Empty;
            var chart2 = string.Empty;
            var chart3 = string.Empty;




            var data = db.MWalletTransactionFees
                .Include(i => i.ETransactionTypeNavigation)
                .AsQueryable();

            if (!String.IsNullOrEmpty(date_from))
            {
                var _date = DateTime.Parse(date_from);
                data = data.Where(i => i.Date >= _date);
            }

            if (!String.IsNullOrEmpty(date_to))
            {
                var _date = DateTime.Parse(date_to);
                data = data.Where(i => i.Date <= _date);
            }

                var base_data=data
                .GroupBy(
                             x => x.ETransactionTypeNavigation.TransactionType,
                            (key, g) => new
                            {
                                type = key,
                                count = g.Count(),
                                fee = g.Sum(i => i.TransactionFee)
                            }
                    ).ToList();

            var total_fees = decimal.Zero;
            foreach (var d in base_data)
            {
                chart1 += $"['{d.type}', {d.count}],";
                chart2 += $"{{ name: '{d.type}', y: {d.count} }},";
                chart3 += $"{{ name: '{d.type}', y: {d.fee} }},";
                total_fees += d.fee;
            }


            ViewBag.chart1 = chart1;
            ViewBag.chart2 = chart2;
            ViewBag.chart3 = chart3;
            ViewBag.date_from = date_from;
            ViewBag.date_to = date_to;
            ViewBag.total_fees = total_fees.ToString("0.00");
            return View();
        }
    }
}
